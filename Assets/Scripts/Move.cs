﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

    public Transform target;

    public Transform point1;
    public Transform point2;

    public float speed;

    private float startTime;
    private float journeyLength;

    private int dir = 0;


    // Use this for initialization
    void Start () {
        startTime = Time.time;
        journeyLength = Vector3.Distance(point1.position, point2.position);
    }
	
	// Update is called once per frame
	void Update () {
        float distCovered = (Time.time - startTime) * speed;
        //float fracJourney = distCovered / journeyLength;

        
        if(dir == 0)
        {
            float fracJourney = distCovered / journeyLength;
            target.position = Vector3.Lerp(point1.position, point2.position, fracJourney);
            if(fracJourney > 1)
            {
                startTime = Time.time;
                journeyLength = Vector3.Distance(point2.position, point1.position);
                dir = 1;
            }
        } else if(dir == 1)
        {
            float fracJourney = distCovered / journeyLength;
            target.position = Vector3.Lerp(point2.position, point1.position, fracJourney);
            if (fracJourney > 1)
            {
                startTime = Time.time;
                journeyLength = Vector3.Distance(point1.position, point2.position);
                dir = 0;
            }
        }

        //Debug.Log(fracJourney);

    }
}
