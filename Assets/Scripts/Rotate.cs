﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

    public enum Direction{Up, Down, Left, Right};

    public float degree = 5f;
    public Transform pos;
    public Direction dir;


	// Use this for initialization
	void Start () {
        //pos = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        switch (dir)
        {
            case Direction.Up:
                pos.transform.Rotate(Vector3.up, degree * Time.deltaTime);
                break;
            case Direction.Down:
                pos.transform.Rotate(Vector3.down, degree * Time.deltaTime);
                break;
            case Direction.Left:
                pos.transform.Rotate(Vector3.left, degree * Time.deltaTime);
                break;
            case Direction.Right:
                pos.transform.Rotate(Vector3.right, degree * Time.deltaTime);
                break;
        }
    }
}
