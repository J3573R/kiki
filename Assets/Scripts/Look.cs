﻿using UnityEngine;
using System.Collections;

public class Look : MonoBehaviour {

    public Transform self;
    public Transform target;

    public float speed = 10.0f;

	// Use this for initialization
	void Start () {
        self = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        var rotation = Quaternion.LookRotation(target.position - self.position);
        transform.rotation = Quaternion.Slerp(self.rotation, rotation, Time.deltaTime * speed);
	}
}
