﻿using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour {

    [SerializeField] private Transform _target;
    [SerializeField] private float _speed = 5f;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        //transform.position = Vector3.MoveTowards(transform.position, _target.transform.position, 0.03f);
        _target.position = new Vector3(_target.position.x + 1f, _target.position.y + 1f, _target.position.z + 1f);
        transform.position = Vector3.Lerp(transform.position, _target.position, _speed * Time.deltaTime);
    }
}
