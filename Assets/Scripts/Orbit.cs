﻿using UnityEngine;
using System.Collections;

public class Orbit : MonoBehaviour {

    [SerializeField] private Transform _target;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = _target.position;
    }
}
