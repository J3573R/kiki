﻿using UnityEngine;
using System.Collections;

public class MovementController : MonoBehaviour {
    
    private float speed = 10f;
	// Use this for initialization
	void Start () {
	}

    // Update is called once per frame
    void Update () {

        Vector3 vTraj = Vector3.zero;
        vTraj.x = Input.GetAxis("Horizontal");
        vTraj.y = Input.GetAxis("Vertical");

        vTraj *= speed * Time.deltaTime;

        transform.position += vTraj;
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("HERE");
    }
}
